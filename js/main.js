var stage;
var KEYCODE_ENTER = 13;     //usefull keycode
var KEYCODE_SPACE = 32;     //usefull keycode
var KEYCODE_UP = 38;        //usefull keycode
var KEYCODE_LEFT = 37;      //usefull keycode
var KEYCODE_RIGHT = 39;     //usefull keycode
var KEYCODE_W = 87;         //usefull keycode
var KEYCODE_A = 65;         //usefull keycode
var KEYCODE_D = 68;         //usefull keycode
//https://github.com/CreateJS/EaselJS/blob/master/examples/Game.html, http://www.gamefromscratch.com/post/2012/12/17/Gamedev-Math-Handling-sprite-based-shooting.aspx,http://www.marketjs.com/,http://melonjs.org/
function init()
{
    stage = new createjs.Stage("lienzo");
    document.onkeydown = handleKeyDown;
    document.onkeyup = handleKeyUp;
    createjs.Ticker.addEventListener("tick", tick);
}
function tick(e){

}
//allow for WASD and arrow control scheme
function handleKeyDown(e) {
    //cross browser issues exist
    if(!e){ var e = window.event; }
    switch(e.keyCode) {
        case KEYCODE_SPACE: shootHeld = true; return false;
        case KEYCODE_A:
        case KEYCODE_LEFT:  lfHeld = true; return false;
        case KEYCODE_D:
        case KEYCODE_RIGHT: rtHeld = true; return false;
        case KEYCODE_W:
        case KEYCODE_UP:    fwdHeld = true; return false;
        // case KEYCODE_ENTER:  if(canvas.onclick == handleClick){ handleClick(); }return false;
    }
}

function handleKeyUp(e) {
    //cross browser issues exist
    if(!e){ var e = window.event; }
    switch(e.keyCode) {
        case KEYCODE_SPACE: shootHeld = false; break;
        case KEYCODE_A:
        case KEYCODE_LEFT:  lfHeld = false; break;
        case KEYCODE_D:
        case KEYCODE_RIGHT: rtHeld = false; break;
        case KEYCODE_W:
        case KEYCODE_UP:    fwdHeld = false; break;
    }
}
function publicarEnFacebook() {
           
    var url = "http://www.facebook.com/sharer.php?";
    var sitio = "http://agencia.pragma.com.co/auteco/pulsar200ns";
    var titulo = "Pulsar%20200NS";
    var descripcion = "Tenemos%20una%20nueva%20misi%C3%B3n%20para%20%20nuestros%20agentes,%20quien%20decida%20aceptarla%20recibir%C3%A1%20una%20200%20NS%20en%20caso%20%20tal%20de%20tener%20%C3%A9xito";
    var thumbnail = sitio+"/img/sharer.png";
    var params = "s=100&p[medium]=106";
    
    params += '&p[title]=';
    params += titulo;
    params += '&p[summary]=';
    params += descripcion;
    params += '&p[url]=';
    params += sitio;
    params += "&p[images][0]=";
    params += thumbnail;
    window.open(url + params, 'sharer', 'toolbar=0,status=0,width=626,height=436');
}

function publicarEnTwitter()
{
	var url = "http://twitter.com/share?";
	var params = 'text=Tenemos%20una%20nueva%20misión%20para%20 nuestros%20agentes,%20quien%20decida%20aceptarla%20recibirá%20una%20200%20NS%20en%20caso%20 tal%20de%20tener%20éxito.';
	params += '%20Link&url=http://goo.gl/ShsW5';
	 window.open(url + params, 'sharer', 'toolbar=0,status=0,width=626,height=436');
}
//http://pinterest.com/pin/create/button/?url={URI-encoded URL of the page to pin}&media={URI-encoded URL of the image to pin}&description={optional URI-encoded description}" class="pin-it-button" count-layout="horizontal
window.onload = init;